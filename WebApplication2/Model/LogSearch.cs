﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Model
{
    public class LogSearch
    {
        public  int Id { get; set; }
        public  string UserId  { get; set; }
        public  string Parametro { get; set; }
        public  System.DateTime DataHora { get; set; }
    }
}
