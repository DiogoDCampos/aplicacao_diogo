﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Repository
{
    public interface IAbstractRepository <T,I> : IDisposable
    {
        T Save(T objeto);
        T Delete(T objeto);
        T Get(I id);
    }
}
