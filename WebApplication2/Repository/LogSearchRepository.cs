﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Model;

namespace WebApplication2.Repository
{
    public class LogSearchRepository : ILogSearchRepository
    {
        private readonly ApplicationDbContext context;

        public LogSearchRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public LogSearch Delete(LogSearch objeto)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public LogSearch Get(int id)
        {
            throw new NotImplementedException();
        }

        public LogSearch Save(LogSearch objeto)
        {

            int? intIdt = context.LogSearch.Max(u => (int?)u.Id);


            intIdt = intIdt==null ? 0 : intIdt+1;
            objeto.Id = intIdt??0;
            context.Add(objeto);
            context.SaveChanges();
            // do stuff
            return objeto;
        }
    }
}
