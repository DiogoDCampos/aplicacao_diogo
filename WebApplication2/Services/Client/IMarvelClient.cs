﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Services.Client
{
    public interface IMarvelClient 
    {
        List<PersonagemModel> FindAllPernosagemByNameStartWith(string name);
    }
}
