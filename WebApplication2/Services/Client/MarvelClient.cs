﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebApplication2.Models;


namespace WebApplication2.Services.Client
{
    public class MarvelClient : IMarvelClient
    {
        private HttpClient client;
        private IConfiguration config;

        public MarvelClient(IConfiguration config)
        {
            this.config = config;
            this.client = new HttpClient();
        }

        public List<PersonagemModel> FindAllPernosagemByNameStartWith(string name)
        {
            var ConfiJson = new JsonSerializerSettings();
            ConfiJson.MissingMemberHandling = MissingMemberHandling.Ignore;

            string ts = DateTime.Now.Ticks.ToString();
            string publicKey = config.GetSection("MarvelComicsAPI:PublicKey").Value;
            string hash = GerarHash(ts, publicKey,
                config.GetSection("MarvelComicsAPI:PrivateKey").Value);

            var builder = new UriBuilder(config.GetSection("MarvelComicsAPI:BaseURL").Value +
                $"characters");
            var query = HttpUtility.ParseQueryString(builder.Query);

            query["ts"] = ts;
            query["apikey"] = publicKey;
            query["hash"] = hash;
            if (name !=null) query["nameStartsWith"] = name;

            builder.Query = query.ToString();


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
      
            HttpResponseMessage response = client.GetAsync(builder.ToString()).Result;

            response.EnsureSuccessStatusCode();
            string conteudo =
                response.Content.ReadAsStringAsync().Result;

            dynamic resultado = JsonConvert.DeserializeObject(conteudo);

            return  JsonConvert.DeserializeObject<List<PersonagemModel>>((resultado.data.results).ToString(), ConfiJson);
        }
        private string GerarHash(
                string ts, string publicKey, string privateKey)
        {
            byte[] bytes =
                Encoding.UTF8.GetBytes(ts + privateKey + publicKey);
            var gerador = MD5.Create();
            byte[] bytesHash = gerador.ComputeHash(bytes);
            return BitConverter.ToString(bytesHash)
                .ToLower().Replace("-", String.Empty);
        }
    }
}
