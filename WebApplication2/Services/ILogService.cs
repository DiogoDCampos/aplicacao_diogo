﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Model;
using WebApplication2.Models;

namespace WebApplication2.Services
{
   public interface ILogService
    {
        LogSearch Save(string Usuario, string Parametro);
    }
}
