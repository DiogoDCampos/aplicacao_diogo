﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Model;
using WebApplication2.Models;
using WebApplication2.Repository;
using WebApplication2.Services.Client;

namespace WebApplication2.Services
{

    public class LogService : ILogService
    {
        private ILogSearchRepository _ILogSearchRepository;

        public LogService(ILogSearchRepository _ILogSearchRepository) {
            this._ILogSearchRepository = _ILogSearchRepository;
        }

        public LogSearch Save(string Usuario, string Parametro)
        {

            LogSearch log = new LogSearch();
            log.Parametro = Parametro;
            log.UserId = Usuario;
            log.DataHora = System.DateTime.Now;
            return _ILogSearchRepository.Save(log);
        }
    }
}
