﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;
using WebApplication2.Services.Client;

namespace WebApplication2.Services
{

    public class MarvelService : IMarvelService
    {
        private IMarvelClient _IMarvelClient;
        private ILogService _ILogService;

        public MarvelService(IMarvelClient _IMarvelClient, ILogService _ILogService)
        {
            this._IMarvelClient = _IMarvelClient;
            this._ILogService = _ILogService;
        }


        public List<PersonagemModel> FindAllPernosagemByNameStartWith(string name, string UserID)
        {
            _ILogService.Save(UserID, name);
            return _IMarvelClient.FindAllPernosagemByNameStartWith(name);
        }
    }
}
