﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication2.Models
{
    public class PersonagemModel
    {
        public PersonagemModel() {
           // this.urls = new List<Url>();
        }
           
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public thumbnail thumbnail { get; set; }
            public Stories stories { get; set; }
            public List <Url> urls { get; set; }


    }
}
