﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class StorieItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
}
