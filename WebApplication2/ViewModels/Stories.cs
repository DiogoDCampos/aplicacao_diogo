﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Stories
    {

        public Stories(){
            this.items = new List<StorieItem>();
           }
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<StorieItem> items { get; set; }
        public int returned { get; set; }
    }
}
