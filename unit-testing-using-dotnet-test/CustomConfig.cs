﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace unit_testing_using_dotnet_test
{
    public class CustomConfig : IConfiguration
    {
        private class Config : IConfigurationSection
        {
            public string this[string key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public string Value { get; set; }

            public string Key { get; set; }

            public string Path { get; set; }

            public IEnumerable<IConfigurationSection> GetChildren()
            {
                throw new NotImplementedException();
            }

            public IChangeToken GetReloadToken()
            {
                throw new NotImplementedException();
            }

            public IConfigurationSection GetSection(string key)
            {
                throw new NotImplementedException();
            }
        }

        public string this[string key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            throw new NotImplementedException();
        }

        public IChangeToken GetReloadToken()
        {
            throw new NotImplementedException();
        }

        public IConfigurationSection GetSection(string key)
        {
            IConfigurationSection obj = new Config();

            if (key == "MarvelComicsAPI:PublicKey")
            {
                obj.Value = "f7b0bce62a89efe69512a6c510c685d2";
            }
            else if (key == "MarvelComicsAPI:PrivateKey")
            {
                obj.Value = "4307018bf3dfe29fee7488757d98c351674140c5";
            }
            else if (key == "ConnectionStrings:DefaultConnection")
            {
                obj.Value = "Integrated Security = SSPI; Persist Security Info = False; Initial Catalog = aspnet - WebApplication2 - B4BC19C1 - AACE - 4D9E-9531 - 6BBF1F58D0CD; Data Source = DESKTOP - QEVMAGH\\SQLEXPRESS";
            }
            else if (key == "MarvelComicsAPI:BaseURL")
            {
                obj.Value = "http://gateway.marvel.com/v1/public/";
            }

            return obj;
        }
    }
}
