﻿using WebApplication2.Data;
using Microsoft.Extensions.Configuration;
using WebApplication2.Services.Client;
using WebApplication2.Services;
using WebApplication2.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace unit_testing_using_dotnet_test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<IMarvelClient, MarvelClient>();
            services.AddScoped<IMarvelService, MarvelService>();
            services.AddScoped<ILogSearchRepository, LogSearchRepository>();
            services.AddScoped<ILogService, LogService>();

        }
    }
}
