﻿
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication2.Data;
using WebApplication2.Repository;
using WebApplication2.Services.Client;

namespace unit_testing_using_dotnet_test
{
    class TestUtils
    {
        public static IConfiguration getConfiguration()
        {
            return new CustomConfig();
        }

        public static IMarvelClient getMarvelClient()
        {
            return new MarvelClient(getConfiguration());
        }
       
    }
}
